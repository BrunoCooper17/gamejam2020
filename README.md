<h1>Mission Codename: A.N.T.H.E.A.L.</h1>

----

<h2>Index</h2>

- [TL;DR](#tldr)
- [Objetivo](#objetivo)
  - [Objetivo abstracto](#objetivo-abstracto)
  - [Objetivo de los jugadores](#objetivo-de-los-jugadores)
- [Mecanicas](#mecanicas)
- [Mecanicas NPI](#mecanicas-npi)
- [Elementos visuales](#elementos-visuales)
- [Artes](#artes)
  - [Estilo visual](#estilo-visual)
  - [Música y efectos](#m%c3%basica-y-efectos)
- [Juegos similares](#juegos-similares)
- [STAFF](#staff)

---

## TL;DR
La reina se va de vacaciones y el hormiguero se queda a cargo de los subditos. Por que videosjuegos,
un humano pisa y destruye el hormiguero.

---

## Objetivo
### Objetivo abstracto
Reparar el hormiguero antes de que la reina vuelva.

### Objetivo de los jugadores
- ***Estaria chido:*** Conseguir reparar el todo el hormiguero antes de un limite de tiempo (**5 mins** como propuesta)
- ***Alternativa:*** Juntar el mayor record posible antes de que termine el tiempo. (**5 mins** como propuesta)

---

## Mecanicas
La mecanica mas importante del juego (EL CORE) es **ARRASTRAR OBJETOS**.

- [x] El jugador controla a una hormiga que se puede desplazar por un mapa (forma laberinto).
- [ ] La hormiga tiene una **barra de desgaste**.
  - [ ]  Se drena al hacer esfuerzo (moverse y arrastrar).
  - [ ]  Se empieza llenar de nuevo al ***descansar*** (No moverse).
  - [ ] Si se acaba descansar por un tiempo (**5 seg** propuesta).
- [x] El jugador ***arrastrar recursos*** (morder y mover) como **alimento**, **hojas**, **ramitas**, etc y ***objetos (estorbos)*** para las hormigas obreras (***hormiguero, centro del mapa u origen***).
- [X] Recursos y objetos **caen** del cielo (***Semialeatorio***).
  - [x] Pueden aplastar hormigas.
  - [x] Si cae sobre otro recurso, lo destruye sobre el que cayó.
  - [x] Pueden obstruir el paso.
    - [x] Puede evitar que una hormiga pase.
    - [ ] Puede evitar que una hormiga pase con carga (pero puede pasar **SIN** carga).
- [x] Si una hormiga (jugador) muere aplastado, hace respone en el hormiguero.
  - [x] Se cuentan con varios puntos aleatorios de respawn en el escenario.

---

## Mecanicas NPI
- [ ] Se pueden empujar hormigas.

---

## Elementos visuales
- [ ] Barra de tiempo restante.
- [ ] Nivel de reparación.
- [ ] Energia de la hormiga.
- [ ] Anuncios de peligros.

---

## Artes
### Estilo visual
- Toon Occidental.
- 2D.
- Flats (no sombras).
### Música y efectos
- Motivadora que aumente en tensión.

---

## Juegos similares
- Fat princess
<br><img src="https://gamespot1.cbsistatic.com/uploads/scale_medium/mig/9/5/9/8/2219598-fatprincesslogo.jpg" height=250>
- Pikmin
<br><img src="https://upload.wikimedia.org/wikipedia/it/5/50/Pikmin_Players_Choice_10020050007.jpg" height=250>
- Overcooked
<br><img src="https://i.ytimg.com/vi/Ww8CAI3yXPI/maxresdefault.jpg" height=250>

---

## STAFF
- Carlos Alberto "Fan" Barcena Martinez.
- Carlos Rodrigo "Fil" Calderon Coronado.
- Daniel "ZeyJin" Lopez Gutierrez.
- Jesús "BrunoCooper" Mastache Caballero.